import os
from uuid import uuid4

from dotenv import load_dotenv
from flask import Flask, request
from flask_basicauth import BasicAuth
from flask_sqlalchemy import SQLAlchemy

load_dotenv()

db = SQLAlchemy()
app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///project.db'
app.config['BASIC_AUTH_USERNAME'] = os.getenv('BASIC_USER')
app.config['BASIC_AUTH_PASSWORD'] = os.getenv('BASIC_PASS')

db.init_app(app)
basic_auth = BasicAuth(app)


class URL(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    target_link = db.Column(db.String(255), nullable=False)
    uuid = db.Column(db.String(255), nullable=False)


with app.app_context():
    db.create_all()


@app.route('/create-link/', methods=['POST'])
@basic_auth.required
def create_link():
    target_link = request.json['target_link']
    url = db.session.query(URL).filter_by(target_link=target_link).first()
    if url is None:
        url = URL(target_link=target_link, uuid=str(uuid4()))
        db.session.add(url)
        db.session.commit()

    return url.uuid, 200


@app.route('/get-link/<uuid>')
@basic_auth.required
def get_link(uuid: str):
    url = db.session.query(URL).filter_by(uuid=uuid).first()
    if url is None:
        return 'Not found', 404

    return url.target_link, 200
